package com.masterpik.taktik.utils.hacks;

import com.masterpik.api.util.UtilNumbers;
import com.masterpik.taktik.Taktik;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class Velocity {

    public static long tickAfterApplying(int ping) {
        try {
            return 10 + Integer.parseInt(Double.toString(UtilNumbers.arround(((double) ping / 50.00), 0)));
        } catch (NumberFormatException e) {
            return 10 + (ping/50);
        }
    }

    public static long tickAfterApplying(Player player) {
        return tickAfterApplying(Taktik.get(player).getPing());
    }


    public static Location getLocByLocAndVector(Location playerLoc, Vector before) {
        playerLoc = playerLoc.clone();
        before = before.clone();

        double ABx = before.getX();
        double ABy = before.getY();
        double ABz = before.getZ();

        double Ax = playerLoc.getX();
        double Ay = playerLoc.getY();
        double Az = playerLoc.getZ();

        double Bx = Ax + ABx;
        double By = Ay + ABy;
        double Bz = Az + ABz;

        return new Location(playerLoc.getWorld(), Bx, By, Bz);
    }

    public static double velocityPower(Vector velocity) {
        return Math.abs(0-velocity.getX()) + Math.abs(0-velocity.getY()) + Math.abs(0-velocity.getZ());
    }
}
