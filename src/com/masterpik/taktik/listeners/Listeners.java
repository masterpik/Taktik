package com.masterpik.taktik.listeners;

public class Listeners {

    public static void init() {
        CombatListeners.init();
        MoveListener.init();
        PlayersListener.init();
    }

}
