package com.masterpik.taktik.listeners;

import com.masterpik.taktik.Taktik;
import com.masterpik.taktik.player.TaktikPlayer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.HashMap;

public class MoveListener implements Listener{

    public static HashMap<TaktikPlayer, ArrayList<BukkitTask>> moves;

    public static void init() {
        Bukkit.getPluginManager().registerEvents(new MoveListener(), Taktik.getPlugin());
        moves = new HashMap<>();
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void PlayerMoveEvent(PlayerMoveEvent event) {
        TaktikPlayer player = Taktik.get(event.getPlayer());

        Vector moveVector = event.getTo().toVector().subtract(event.getFrom().toVector());
        player.setVelocity(moveVector);

        if (!moveVector.equals(new Vector(0,0,0))) {

            if (!moves.containsKey(player)) moves.put(player, new ArrayList<>());

            for (BukkitTask tasks : moves.get(player)) {
                tasks.cancel();
            }

            moves.get(player).removeIf(p -> (!Bukkit.getScheduler().isCurrentlyRunning(p.getTaskId()) || Bukkit.getScheduler().isQueued(p.getTaskId())));

            if (!player.isMoving()) player.setMoving(true);

            moves.get(player).add(Bukkit.getScheduler().runTaskLater(Taktik.getPlugin(), new Runnable() {
                @Override
                public void run() {

                    if (player.isMoving()) {
                        player.setMoving(false);
                        player.setVelocity(new Vector(0, 0, 0));
                    }

                }
            }, 2));
        }
    }
}
