package com.masterpik.taktik.listeners;

import com.masterpik.api.util.UtilVector;
import com.masterpik.taktik.Taktik;
import com.masterpik.taktik.player.TaktikPlayer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.HashMap;

public class CombatListeners implements Listener{

    public static HashMap<TaktikPlayer, ArrayList<BukkitTask>> damages;

    public static void init(){
        Bukkit.getPluginManager().registerEvents(new CombatListeners(), Taktik.getPlugin());
        damages = new HashMap<>();
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void EntityDamageByEntityEvent(EntityDamageByEntityEvent event) {
        if (event.getEntity() instanceof Player) {
            TaktikPlayer player = Taktik.get((Player) event.getEntity());

            player.setVelocityYaw(UtilVector.getYawLocations(event.getDamager().getLocation(), event.getEntity().getLocation()));
            player.setCombatSoon(true);

            if (!damages.containsKey(player)) damages.put(player, new ArrayList<>());

            for (BukkitTask tasks : damages.get(player)) {
                tasks.cancel();
            }

            damages.get(player).removeIf(p -> (!Bukkit.getScheduler().isCurrentlyRunning(p.getTaskId()) || Bukkit.getScheduler().isQueued(p.getTaskId())));

            damages.get(player).add(Bukkit.getScheduler().runTaskLater(Taktik.getPlugin(), new Runnable() {
                @Override
                public void run() {
                    player.setVelocityYaw(360);
                    if (player.isCombatSoon()) {
                        player.setCombatSoon(false);
                    }
                }
            }, 2));
        }
    }
}
