package com.masterpik.taktik.listeners;

import com.masterpik.taktik.Taktik;
import com.masterpik.taktik.player.PlayersManager;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayersListener implements Listener {

    public static void init() {
        Bukkit.getPluginManager().registerEvents(new PlayersListener(), Taktik.getPlugin());
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void PlayerJoinEvent(PlayerJoinEvent event) {
        PlayersManager.playerJoin(event.getPlayer().getUniqueId());
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void PlayerQuitEvent(PlayerQuitEvent event) {
        PlayersManager.playerQuit(event.getPlayer().getUniqueId());
    }



}
