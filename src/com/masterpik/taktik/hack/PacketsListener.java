package com.masterpik.taktik.hack;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.ListenerOptions;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import com.masterpik.taktik.Taktik;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class PacketsListener extends PacketAdapter {

    private PacketType type;
    private int amount;

    public PacketsListener(PacketType type) {
        super(Taktik.getPlugin(), PacketType.values());
        this.type = type;
        this.amount = 0;
    }

    public void addAmount() {
        this.amount = this.amount + 1;
    }
    public PacketType getType() {
        return type;
    }
    public void setType(PacketType type) {
        this.type = type;
    }
    public int getAmount() {
        return amount;
    }
    public void setAmount(int amount) {
        this.amount = amount;
    }

    private static HashMap<UUID, HashMap<PacketType, PacketsListener>> packets;

    public PacketsListener(AdapterParameteters params) {
        super(params);
    }

    public PacketsListener(Plugin plugin, PacketType... types) {
        super(plugin, types);
    }

    public PacketsListener(Plugin plugin, Iterable<? extends PacketType> types) {
        super(plugin, types);
    }

    public PacketsListener(Plugin plugin, ListenerPriority listenerPriority, Iterable<? extends PacketType> types) {
        super(plugin, listenerPriority, types);
    }

    public PacketsListener(Plugin plugin, ListenerPriority listenerPriority, Iterable<? extends PacketType> types, ListenerOptions... options) {
        super(plugin, listenerPriority, types, options);
    }

    public PacketsListener(Plugin plugin, ListenerPriority listenerPriority, PacketType... types) {
        super(plugin, listenerPriority, types);
    }

    public static void init() {
        Taktik.getProtocolManager().addPacketListener(new PacketsListener(Taktik.getPlugin(), ListenerPriority.NORMAL, PacketType.Play.Client.getInstance().values()));
        packets = new HashMap<>();

        int timer = 1;

        Bukkit.getScheduler().runTaskTimer(Taktik.getPlugin(), new Runnable() {
            @Override
            public void run() {

                boolean color = true;
                String c = "";
                for (UUID p : packets.keySet()) {

                    if (Bukkit.getPlayer(p) != null) {

                        color = !color;

                        if (color) {
                            c = "\u001B[33m";
                        } else {
                            c = "\u001B[31m";
                        }

                        HashMap<PacketType, PacketsListener> map = packets.get(p);

                        String msg = c+"[TAKTIK] All Packets last " + timer + "s for player " + Bukkit.getPlayer(p).getName().toUpperCase() + " (";

                        for (PacketType l1 : map.keySet()) {
                            PacketsListener l = packets.get(p).get(l1);

                            if (l.getAmount() > 0) {
                                msg = msg + l1.name() + " \u001B[0m" + l.getAmount() + c+" | ";

                                l.setAmount(0);
                            }
                        }

                        msg = msg+")\u001B[0m";
                        Bukkit.getLogger().info(msg);
                    }
                }
                Bukkit.getLogger().info(" ");



            }
        }, 100, 20*timer);
    }

    @Override
    public void onPacketReceiving(PacketEvent event) {

        Player player = event.getPlayer();

        PacketType t = event.getPacketType();

        ArrayList<PacketType> nop = new ArrayList<>();
        /*nop.add(PacketType.Play.Client.POSITION);
        nop.add(PacketType.Play.Client.POSITION_LOOK);*/

        if (!nop.contains(event.getPacketType())) {
            if (!packets.containsKey(player.getUniqueId())) {
                packets.put(player.getUniqueId(), new HashMap<>());
            }

            if (!packets.get(player.getUniqueId()).containsKey(t)){
                packets.get(player.getUniqueId()).put(t, new PacketsListener(t));
            }

            packets.get(player.getUniqueId()).get(t).addAmount();

            //Bukkit.getLogger().info("[TAKTIK] <<-- (player : " + event.getPlayer().getName() + " | packet type : " + event.getPacketType().name() + ")");
        }
    }

}
