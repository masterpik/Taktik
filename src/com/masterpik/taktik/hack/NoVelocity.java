package com.masterpik.taktik.hack;

import com.masterpik.api.util.UtilLocation;
import com.masterpik.api.util.UtilVector;
import com.masterpik.api.util.location.Yaws;
import com.masterpik.taktik.Taktik;
import com.masterpik.taktik.player.PlayersManager;
import com.masterpik.taktik.player.TaktikPlayer;
import com.masterpik.taktik.utils.hacks.Velocity;
import net.minecraft.server.v1_9_R1.EntityPlayer;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_9_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerVelocityEvent;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.util.Vector;

import java.util.ArrayList;

public class NoVelocity implements Listener{

    public static void init() {
        Bukkit.getPluginManager().registerEvents(new NoVelocity(), Taktik.getPlugin());

    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void PlayerVelocityEvent(PlayerVelocityEvent event) {
        Player player = event.getPlayer();
        TaktikPlayer Tplayer = Taktik.get(player);

        int ping = Tplayer.getPing();

        if (ping < 10000
                && player.isValid()
                && !player.isInsideVehicle()
                && !player.hasPotionEffect(PotionEffectType.ABSORPTION)
                && !player.hasPotionEffect(PotionEffectType.POISON)
                && !player.hasPotionEffect(PotionEffectType.WITHER)
                && player.getLastDamageCause() != null
                && player.getLastDamageCause().getCause() != null
                && !player.getLastDamageCause().getCause().equals(EntityDamageEvent.DamageCause.FALL)
                && !player.getLastDamageCause().getCause().equals(EntityDamageEvent.DamageCause.FLY_INTO_WALL)
                && !player.getLastDamageCause().getCause().equals(EntityDamageEvent.DamageCause.DROWNING)
                && !player.getLastDamageCause().getCause().equals(EntityDamageEvent.DamageCause.FIRE)
                && !player.getLastDamageCause().getCause().equals(EntityDamageEvent.DamageCause.FIRE_TICK)
                && !player.getLastDamageCause().getCause().equals(EntityDamageEvent.DamageCause.POISON)
                && !player.getLastDamageCause().getCause().equals(EntityDamageEvent.DamageCause.SUFFOCATION)
                && !player.getLastDamageCause().getCause().equals(EntityDamageEvent.DamageCause.WITHER)
                && !player.getLastDamageCause().getCause().equals(EntityDamageEvent.DamageCause.STARVATION)
                && !player.getLastDamageCause().getCause().equals(EntityDamageEvent.DamageCause.LAVA)
                && !player.getLastDamageCause().getCause().equals(EntityDamageEvent.DamageCause.LIGHTNING)
                && !player.getLastDamageCause().getCause().equals(EntityDamageEvent.DamageCause.SUICIDE)
                && !player.getLastDamageCause().getCause().equals(EntityDamageEvent.DamageCause.VOID)
                && !player.getGameMode().equals(GameMode.SPECTATOR)
                && !player.getGameMode().equals(GameMode.CREATIVE)
                && !player.isSleeping()
                && !player.isFlying()
                && !player.isDead()) {

            Location loc1 = player.getLocation().clone();
            Vector velocity = event.getVelocity().clone();
            Vector real = velocity.add(Tplayer.getVelocity().clone()).add(player.getVelocity().clone());

            Location goodLoc = (new Vector (player.getLocation().getX(), player.getLocation().getY(), player.getLocation().getZ())).add(real).toLocation(player.getWorld(), player.getLocation().getYaw(), player.getLocation().getPitch());

            boolean isMoving = Tplayer.isMoving();
            float knockYaw = UtilVector.getYaw(real);
            if ((real.getX() == 0 && real.getZ() == 0) && Tplayer.getVelocityYaw() != 360) {
                knockYaw = Tplayer.getVelocityYaw();
            }
            boolean isBlock = Tplayer.isBlock(Yaws.getNextYaw(knockYaw));

            double power = Velocity.velocityPower(real);

            CraftPlayer cp = (CraftPlayer) player;
            EntityPlayer ep = cp.getHandle();
            Vector nmsVelocity = new Vector(ep.motX, ep.motY, ep.motZ);

            /*Bukkit.getLogger().info("[TAKTIK] Velocity Event : (player : " + event.getPlayer().getName() + " | isMoving : " + Tplayer.isMoving() + " | isBlock : " + isBlock + " | ping : " + Taktik.get(player).getPing() +")");
            Bukkit.getLogger().info("[TAKTIK]       Event Velocity : " + event.getVelocity().toString());
            Bukkit.getLogger().info("[TAKTIK]       Player Velocity : " + player.getVelocity().toString());
            Bukkit.getLogger().info("[TAKTIK]       Move Velocity : " + Tplayer.getVelocity().toString());
            Bukkit.getLogger().info("[TAKTIK]       Real Velocity : " + real.toString());
            Bukkit.getLogger().info("[TAKTIK]       NMS Velocity : " + nmsVelocity.toString() + "");
            Bukkit.getLogger().info("[TAKTIK]       POWER : " + power + "\n ");

            player.sendMessage("\n§a[TAKTIK] Velocity Power : §l" + power);*/

            /*event.setCancelled(true);
            player.teleport(goodLoc);*/

            if (!isBlock) {

                if (!isMoving) {
                    Bukkit.getScheduler().runTaskLater(Taktik.getPlugin(), new Runnable() {
                        @Override
                        public void run() {
                            Location loc2 = player.getLocation();

                            double diff = loc2.distanceSquared(loc1);

                            Vector apply = player.getLocation().clone().subtract(loc1.clone()).toVector();
                            double applyPower = Velocity.velocityPower(apply);

                            double bad = 1.0;
                            //player.sendMessage("§e[TAKTIK] Apply Power : §l"+applyPower);

                            if (applyPower < power) {
                                double cheatPoints = power - applyPower;
                                //Tplayer.addNoVelocityCP(cheatPoints);
                                if (true /*Tplayer.getStatu().getId() == (byte) 2*/) {
                                    Bukkit.getLogger().info("[TAKTIK] CHEAT "+player.getName()+" ! §oANTI-VELOCITY§r §c | CHEAT POINTS : §l" + cheatPoints);
                                    player.sendMessage("[TAKTIK] §c§lCHEAT ! §oANTI-VELOCITY§r §c | CHEAT POINTS : §l" + cheatPoints);
                                    player.sendMessage("[TAKTIK] §c§oAnti Cheat en cours de développement, ne prennez donc pas compte de cette alerte");
                                }
                            }

                        }
                    }, Velocity.tickAfterApplying(ping));
                }

            }
        }
    }



    @EventHandler
    public void PlayerMoveEvent(PlayerMoveEvent event) {
        /*if (event.getPlayer().getLocation().getBlock().getType().equals(Material.REDSTONE_WIRE)) {
            event.getPlayer().setVelocity(event.getPlayer().getVelocity().zero().setY(1));
        }*/
    }

}
