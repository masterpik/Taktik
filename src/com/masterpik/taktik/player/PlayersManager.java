package com.masterpik.taktik.player;

import com.masterpik.api.players.Players;
import com.masterpik.taktik.Taktik;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.UUID;

public class PlayersManager {

    public static HashMap<UUID, TaktikPlayer> players;

    public static void init() {
        players = new HashMap<>();

        Bukkit.getScheduler().runTaskLater(Taktik.getPlugin(), new Runnable() {
            @Override
            public void run() {
                for (Player player : Bukkit.getOnlinePlayers()) {
                    playerJoin(player.getUniqueId());
                }
            }
        }, 200);
    }

    public static void playerJoin(UUID uuid) {
        if (Players.players.containsKey(uuid)) {
            players.put(uuid, new TaktikPlayer(Players.players.get(uuid)));
        }
    }

    public static void playerQuit(UUID uuid) {
        if (players.containsKey(uuid)) {
            players.remove(uuid);
        }
    }

}
