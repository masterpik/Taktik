package com.masterpik.taktik.player;

import com.masterpik.api.players.MasterpikPlayer;
import com.masterpik.api.util.location.Yaws;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.ArrayList;

public class TaktikPlayer extends MasterpikPlayer {

    private boolean moving;

    private Vector velocity;
    private float velocityYaw;

    private boolean combatSoon;

    //CHEAT POINTS
    private double noVelocityCP;

    public TaktikPlayer(MasterpikPlayer clone) {
        super(clone);
        this.velocity = this.getBukkitPlayer().getVelocity().clone();
        this.moving = false;
        this.velocityYaw = 360;
        this.combatSoon = false;
        this.noVelocityCP = -1;
    }

    @Override
    public Player getPlayer() {
        return super.getBukkitPlayer();
    }

    public Vector getVelocity() {
        return velocity;
    }
    public void setVelocity(Vector velocity) {
        this.velocity = velocity;
    }

    public boolean isMoving() {
        return moving;
    }
    public void setMoving(boolean moving) {
        this.moving = moving;
    }

    public float getVelocityYaw() {
        return velocityYaw;
    }
    public void setVelocityYaw(float velocityYaw) {
        this.velocityYaw = velocityYaw;
    }


    public boolean isCombatSoon() {
        return combatSoon;
    }
    public void setCombatSoon(boolean combatSoon) {
        this.combatSoon = combatSoon;
    }

    public double getNoVelocityCP() {
        return noVelocityCP;
    }
    public void addNoVelocityCP(double noVelocityCP) {
        this.noVelocityCP = this.noVelocityCP + noVelocityCP;
    }
    public void setNoVelocityCP(double noVelocityCP) {
        this.noVelocityCP = noVelocityCP;
    }

    public boolean isBlock(Yaws face) {

        Location location = this.getBukkitPlayer().getLocation();
        Block playerBlock = location.getBlock();
        ArrayList<Yaws> facesToCheck = Yaws.facesToCheck(face);

        boolean checkNext = false;

        for (Yaws check : facesToCheck) {
            switch (check) {
                case SOUTH:
                    if (location.getZ() >= 0) {
                        if (location.getZ() > (location.getBlockZ() + 0.700))
                            playerBlock = playerBlock.getRelative(check.getBlockFace());
                    } else {
                        if (location.getZ() > (location.getBlockZ() + 0.700))
                            playerBlock = playerBlock.getRelative(check.getBlockFace());
                    }
                    break;
                case WEST:
                    if (location.getX() > 0) {
                        if (location.getX() < (location.getBlockX() + 0.300))
                            playerBlock = playerBlock.getRelative(check.getBlockFace());
                    } else {
                        if (location.getX() < (location.getBlockX() + 0.300))
                            playerBlock = playerBlock.getRelative(check.getBlockFace());
                    }
                    break;
                case NORTH:
                    if (location.getZ() > 0) {
                        if (location.getZ() < (location.getBlockZ() + 0.300))
                            playerBlock = playerBlock.getRelative(check.getBlockFace());
                    } else {
                        if (location.getZ() < (location.getBlockZ() + 0.300))
                            playerBlock = playerBlock.getRelative(check.getBlockFace());
                    }
                    break;
                case EAST:
                    if (location.getX() >= 0) {
                        if (location.getX() > (location.getBlockX() + 0.700))
                            playerBlock = playerBlock.getRelative(check.getBlockFace());
                    } else {
                        if (location.getX() > (location.getBlockX() + 0.700))
                            playerBlock = playerBlock.getRelative(check.getBlockFace());
                    }
                    break;
            }
        }

        boolean block = false;

        for (Yaws check : facesToCheck) {
            if (!block) {
                block = playerBlock.getRelative(check.getBlockFace()).getType().isSolid();
            }

            if (!block) {
                block = playerBlock.getRelative(BlockFace.UP).getRelative(check.getBlockFace()).getType().isSolid();
            }
        }


        if (!block) {
            block = (playerBlock.getType().isSolid() || location.getBlock().getType().isSolid());
        }

        return block;
    }
}


