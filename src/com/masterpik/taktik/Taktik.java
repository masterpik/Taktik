package com.masterpik.taktik;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.masterpik.api.players.Players;
import com.masterpik.taktik.hack.Hacks;
import com.masterpik.taktik.listeners.Listeners;
import com.masterpik.taktik.player.PlayersManager;
import com.masterpik.taktik.player.TaktikPlayer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class Taktik extends JavaPlugin {

    private static Taktik plugin;
    private static ProtocolManager protocolManager;

    @Override
    public void onEnable() {
        plugin = this;
        protocolManager = ProtocolLibrary.getProtocolManager();

        Listeners.init();
        PlayersManager.init();
        Hacks.init();
    }

    public static Taktik getPlugin() {
        return plugin;
    }

    public static ProtocolManager getProtocolManager() {
        return protocolManager;
    }

    public static TaktikPlayer get(Player player) {
        if (!Players.players.containsKey(player.getUniqueId())) {
            Players.playerJoin(player.getUniqueId());
            PlayersManager.playerJoin(player.getUniqueId());
        }

        if (!PlayersManager.players.containsKey(player.getUniqueId())) {
            PlayersManager.playerJoin(player.getUniqueId());
        }

        return PlayersManager.players.get(player.getUniqueId());
    }
}
